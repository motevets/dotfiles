#!/bin/bash

set -ex

sudo apt-get -y update

# kernel
sudo apt-get install linux-generic-lts-vivid linux-generic linux-image-extra-virtual-lts-vivid

# packages
sudo apt-get install -y tmux vim git
## aws utilities
sudo apt-get install -y python-pip
sudo pip install awscli awsebcli
## neovim
sudo apt-get install -y software-properties-common
sudo add-apt-repository -y ppa:neovim-ppa/unstable
sudo apt-get install -y neovim
### bootstrap the vim bundler
if [ ! -e "$HOME/.config/nvim/bundle/repos/github.com/Shougo/dein.vim/" ]; then
  wget -O - https://raw.githubusercontent.com/Shougo/dein.vim/master/bin/installer.sh | bash -s "$HOME/.config/nvim/bundle"
fi
## docker
if ! which docker; then
  sudo apt-get install -y apparmor
  sudo apt-key adv --keyserver hkp://p80.pool.sks-keyservers.net:80 --recv-keys 58118E89F3A912897C070ADBF76221572C52609D
  echo "deb https://apt.dockerproject.org/repo ubuntu-trusty main" | sudo tee /etc/apt/sources.list.d/docker.list
  sudo apt-get update
  apt-cache policy docker-engine
  sudo apt-get install docker-engine
  sudo service docker start
  sudo docker run hello-world
fi
### docker-compose
sudo curl -L https://github.com/docker/compose/releases/download/1.7.0/docker-compose-`uname -s`-`uname -m` > /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose

# bash-git-prompt
if [ ! -e ".bash-git-prompt" ]; then
  git clone https://github.com/magicmonty/bash-git-prompt.git .bash-git-prompt --depth=1
fi

# install homeshick
if [ ! -e "$HOME/.homesick/repos/homeshick" ]; then
  git clone git://github.com/andsens/homeshick.git $HOME/.homesick/repos/homeshick
fi
source $HOME/.homesick/repos/homeshick/homeshick.sh
## trust github
mkdir -p .ssh
cat <<EOF >> .ssh/known_hosts
|1|6WX8FDwncDK8tfyfkLLbvyepVRw=|15RHFpHg3GHML7eJqvNL/yVYChI= ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAQEAq2A7hRGmdnm9tUDbO9IDSwBK6TbQa+PXYPCPy6rbTrTtw7PHkccKrpp0yVhp5HdEIcKr6pLlVDBfOLX9QUsyCOV0wzfjIJNlGEYsdlLJizHhbn2mUjvSAHQqZETYP81eFzLQNnPHt4EVVUh7VfDESU84KezmD5QlWpXLmvU31/yMf+Se8xhHTvKSCZIFImWwoG6mbUoWf9nzpIoaSjB+weqqUUmpaaasXVal72J+UX2B+2RPW3RcT0eOzQgqlJL3RKrTJvdsjE3JEAvGq3lGHSZXy28G3skua2SmVi/w4yCE6gbODqnTWlg7+wC604ydGXA8VJiS5ap43JXiUFFAaQ==
|1|cccEMXs7ur0u/JXs4NQYv4A9Xb8=|Pddv+wa776NKeZ4v1yMn1cZWt4s= ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAQEAq2A7hRGmdnm9tUDbO9IDSwBK6TbQa+PXYPCPy6rbTrTtw7PHkccKrpp0yVhp5HdEIcKr6pLlVDBfOLX9QUsyCOV0wzfjIJNlGEYsdlLJizHhbn2mUjvSAHQqZETYP81eFzLQNnPHt4EVVUh7VfDESU84KezmD5QlWpXLmvU31/yMf+Se8xhHTvKSCZIFImWwoG6mbUoWf9nzpIoaSjB+weqqUUmpaaasXVal72J+UX2B+2RPW3RcT0eOzQgqlJL3RKrTJvdsjE3JEAvGq3lGHSZXy28G3skua2SmVi/w4yCE6gbODqnTWlg7+wC604ydGXA8VJiS5ap43JXiUFFAaQ==
EOF
## install dotfiles
if [ ! -e "$HOME/.homesick/repos/dotfiles" ]; then
  homeshick --batch clone https://gitlab.com/motevets/dotfiles.git
fi
homeshick link --force

# fin.
echo "Ready to go! You may want to reboot or re-bash."
