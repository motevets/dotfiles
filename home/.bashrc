export TERM="xterm-256color"

# git prompt
source ~/.bash-git-prompt/gitprompt.sh
GIT_PROMPT_ONLY_IN_REPO=1

source "$HOME/.homesick/repos/homeshick/homeshick.sh"
# The next line updates PATH for the Google Cloud SDK.
source '/home/motevets/Downloads/google-cloud-sdk/path.bash.inc'

# The next line enables shell command completion for gcloud.
source '/home/motevets/Downloads/google-cloud-sdk/completion.bash.inc'

export GOPATH="$HOME/go"
export PATH="$GOPATH/bin:$PATH"
