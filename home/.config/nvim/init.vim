if &compatible
  set nocompatible               " Be iMproved
endif

""""""""""""""""""""""""
" Base Vim Preferences "
""""""""""""""""""""""""
syntax on
colorscheme morning
set number
set tabstop=2
set shiftwidth=2
set expandtab

""""""""""""""""""""""
" Keyboard Shortcuts "
""""""""""""""""""""""

"""""""""""""""""""
" Install Plugins "
"""""""""""""""""""
" Required:
set runtimepath^=$HOME/.config/nvim/bundle/repos/github.com/Shougo/dein.vim

" Required:
call dein#begin(expand("$HOME/.config/nvim/bundle"))

" Let dein manage dein
" Required:
call dein#add('Shougo/dein.vim')

" Add or remove your plugins here:
call dein#add('scrooloose/nerdtree')
call dein#add('rking/ag.vim')
call dein#add('tpope/vim-fugitive')
call dein#add('Shougo/neosnippet.vim')
call dein#add('Shougo/neosnippet-snippets')

" You can specify revision/branch/tag.
call dein#add('Shougo/vimshell', { 'rev': '3787e5' })

" Required:
call dein#end()

" Required:
filetype plugin indent on

" If you want to install not installed plugins on startup.
if dein#check_install()
  call dein#install()
endif
